<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MailerController.
 */
class MailerController extends AbstractController
{
    /**
     * @Route("/email", name="mail_send")
     */
    public function sendEmail(MailerInterface $mailer): Response
    {
        $email = (new Email())
            ->from('chichkinvs.symfony@gmail.com')
            ->to('it@hemafund.com')
//            ->cc('cc@example.com')
//            ->bcc('bcc@example.com')
//            ->replyTo('fabien@example.com')
//            ->priority(Email::PRIORITY_HIGH)
            ->subject('Test from Symfony EMail')
            ->text('Hello!')
//            ->attachFromPath('/home/chivs/Загрузки/Neuschwanstein-1.jpg')
            ->html('<p>Hello, World!</p>');

        $mailer->send($email);

        return $this->json(['message' => 'I hope, that email sent successfully']);
    }
}
