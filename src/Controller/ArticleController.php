<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController.
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/article/{_locale}", name="article_index", requirements={"_locale"="en|ru"})
     */
    public function indexAction(Request $request, PaginatorInterface $paginator)
    {
        $query = $this->getDoctrine()
            ->getRepository(Article::class)
            ->createQueryBuilder('c');

        $articles = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/ ,
            $request->getSession()->get('items', $request->query->get('items', 5))
        );

        return $this->render('article/index.html.twig', ['articles' => $articles]);
    }

//    {
//        $em = $this->getDoctrine()->getManager();
//        $articles = $em->getRepository(Article::class)->findAll();
//
//        return $this->render('article/index.html.twig', ['articles' => $articles]);
//    }

    /**
     * @Route("/article/{_locale}/last", name="article_last", requirements={"_locale"="en|ru"})
     */
    public function lastAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository(Article::class)->findAll();
        $article = max($articles);

        return $this->render('article/last.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/article/{_locale}/create", name="article_create", requirements={"_locale"="en|ru"})
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/create.html.twig', ['article' => $article, 'form' => $form->createView()]);
    }

    /**
     * @Route("/article/{_locale}/edit/{id}", name="article_edit", requirements={"_locale"="en|ru"})
     *
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID '.$id.' not found!');
        }

        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'editForm' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/article/{_locale}/delete/{id}", name="article_delete", requirements={"_locale"="en|ru"})
     *
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID '.$id.' not found!');
        }

//           if (isset($_POST['YES'])) {
        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('article_index');
        /*            }

                    if (isset($_POST['NO'])) {
                        return $this->redirectToRoute('article_index');
                    }

        */ return $this->render('article/delete.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/article/locale/{locale}", name="article_locale")
     */
    public function changeLocaleAction(Request $request, string $locale)
    {
        $request->getSession()->set('_locale', $locale);

        return $this->redirectToRoute('article_index', ['_locale' => $locale]);
    }

//    /**
//     * @Route("/article/locale/{_locale}", name="article_locale")
//     */
//    public function changeLocaleAction(Request $request, string $locale)
//    {
//        $request->getSession()->set('_locale', $locale);
//
//        $referer = $request->headers->get('referer');
//
//        if (null == $referer) {
//            return $this->redirectToRoute('login', ['_locale' => $locale]);
//        } else {
//            return new RedirectResponse($referer);
//        }
//    }

    /*
 * @Route("/product", name="product_index")
 */
//    public function logoutAction()
//    {
//        return $this->redirectToRoute('login');
//    }
}
