<?php

namespace App\Controller;

use App\Entity\User;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Knp\Snappy\Pdf;
use Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExportController.
 */
class ExportController extends AbstractController
{
    /**
     * @Route("/pdf", name="pdf_create")
     */
    public function pdfCteate()
    {
        $mpdf = new Mpdf();
        $mpdf->WriteHTML(file_get_contents('https://itc.ua/'));
        $mpdf->Output(__DIR__.'/../../public/pdf/info.pdf', false);

        $response = new BinaryFileResponse(__DIR__.'/../../public/pdf/info.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'info.pdf');

        return $response;
    }

    /**
     * @Route("/xls", name="xls_create")
     */
    public function xlsCreate()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World!');

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__.'/../../public/xls/info.xlsx');

        $response = new BinaryFileResponse(__DIR__.'/../../public/xls/info.xlsx');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'info.xlsx');

        return $response;
    }

    /**
     * @Route("/xls/download", name="download_user_xls")
     */
    public function downloadUsers()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();

        $excel = new Spreadsheet();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();

        $sheet->setTitle('Users');

        $row = 3;
        $style = [
            'borders' => [
                'allborders' => [
                    'style' => Border::BORDER_THIN,
                ],
            ],
        ];

        /** @var User $user */
        foreach ($users as $user) {
            $sheet
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'Name')
                ->setCellValue('C1', 'Password')
                ->setCellValue('D1', 'Role')
                ->setCellValue('E1', 'Locale')

                ->setCellValue('A'.$row, $user->getId())
                ->setCellValue('B'.$row, $user->getUsername())
                ->setCellValue('C'.$row, $user->getPassword())
                ->setCellValue('D'.$row, $user->getRolesStr())
                ->setCellValue('E'.$row, $user->getUserLocale())
            ;
            ++$row;
        }
        --$row;
        $sheet->getStyle('B2:P'.$row)->applyFromArray($style);
        $objWriter = new Xls($excel);
        $filename = __DIR__.'/../../public/xls/users.xls';
        $objWriter->save($filename);

        return new BinaryFileResponse($filename);
    }

    /**
     * @Route("/qr", name="qr_code_create").
     */
    public function QRCodeCreate()
    {
        $writer = new PngWriter();

        $qrCode = QrCode::create('https://football.ua/')
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);
        $result->saveToFile(__DIR__.'/../../public/qr/qr_code_new.png');

        $response = new BinaryFileResponse(__DIR__.'/../../public/qr/qr_code_new.png');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'qrcode.png');

        return $response;
    }
}
