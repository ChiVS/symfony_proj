<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/product/{_locale}", name="product_index", requirements={"_locale"="en|ru"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository(Product::class)->findAll();

        return $this->render('product/index.html.twig', ['products' => $products]);
    }

    /**
     * @Route("/product/{_locale}/create", name="product_create", requirements={"_locale"="en|ru"})
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/create.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
            ]);
    }

    /**
     * @Route("/product/{_locale}/edit/{id}", name="product_edit", requirements={"_locale"="en|ru"})
     */
    public function editAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            throw $this->createNotFoundException('Product with ID '.$id.' not found!');
        }

        $editForm = $this->createForm(ProductType::class, $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'editForm' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/product/{_locale}/delete/{id}", name="product_delete", requirements={"_locale"="en|ru"})
     */
    public function deleteAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            throw $this->createNotFoundException('Product with ID'.$id.'not found!');
        }

        //           if (isset($_POST['YES'])) {
        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('product_index');
        /*            }

                    if (isset($_POST['NO'])) {
                        return $this->redirectToRoute('article_index');
                    }

        */ return $this->render('product/delete.html.twig', ['product' => $product]);
    }
}
