<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassSymfonyRepository")
 */
class ClassSymfony
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NamespaceSymfony", inversedBy="classesSymfony")
     * @ORM\JoinColumn(nullable=false)
     */
    private $namespaceSymfony;

    /**
     * ClassSymfony constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): ClassSymfony
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): ClassSymfony
    {
        $this->url = $url;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): ClassSymfony
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getNamespaceSymfony(): NamespaceSymfony
    {
        return $this->namespaceSymfony;
    }

    public function setNamespaceSymfony(NamespaceSymfony $namespaceSymfony): ClassSymfony
    {
        $this->namespaceSymfony = $namespaceSymfony;

        return $this;
    }
}
