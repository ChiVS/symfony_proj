<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NamespaceSymfonyRepository")
 */
class NamespaceSymfony
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NamespaceSymfony")
     * @ORM\JoinColumn (name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InterfaceSymfony", mappedBy="namespaceSymfony")
     */
    private $interfacesSymfony;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClassSymfony", mappedBy="namespaceSymfony")
     */
    private $classesSymfony;

    /**
     * NamespaceSymfony constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->interfacesSymfony = new ArrayCollection();
        $this->classesSymfony = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(NamespaceSymfony $NamespaceSymfony)
    {
        $this->parent = $NamespaceSymfony;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Collection|InterfaceSymfony[]
     */
    public function getInterfacesSymfony()
    {
        return $this->interfacesSymfony;
    }

    public function addInterfacesSymfony($interfacesSymfony)
    {
        if (!$this->interfacesSymfony->contains($interfacesSymfony)) {
            $this->interfacesSymfony[] = $interfacesSymfony;
            $interfacesSymfony->setNamespaceSymfony($this);
        }

        return $this;
    }

    public function removeInterfacesSymfony($interfacesSymfony)
    {
        if ($this->interfacesSymfony->contains($interfacesSymfony)) {
            $this->interfacesSymfony->removeElement($interfacesSymfony);
            // set the owning side to null (unless already changed)
            if ($interfacesSymfony->getNamespaceSymfony() === $this) {
                $interfacesSymfony->setNamespaceSymfony(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClassSymfony[]
     */
    public function getClassesSymfony()
    {
        return $this->classesSymfony;
    }

    public function addClassesSymfony($classesSymfony)
    {
        if (!$this->classesSymfony->contains($classesSymfony)) {
            $this->classesSymfony[] = $classesSymfony;
            $classesSymfony->setNamespaceSymfony($this);
        }

        return $this;
    }

    public function removeClassesSymfony($classesSymfony)
    {
        if ($this->classesSymfony->contains($classesSymfony)) {
            $this->classesSymfony->removeElement($classesSymfony);
            // set the owning side to null (unless already changed)
            if ($classesSymfony->getNamespaceSymfony() === $this) {
                $classesSymfony->setNamespaceSymfony(null);
            }
        }

        return $this;
    }
}
