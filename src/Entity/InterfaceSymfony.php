<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InterfaceSymfonyRepository")
 */
class InterfaceSymfony
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NamespaceSymfony", inversedBy="interfacesSymfony")
     * @ORM\JoinColumn(nullable=false)
     */
    private $namespaceSymfony;

    /**
     * InterfaceSymfony constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): InterfaceSymfony
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): InterfaceSymfony
    {
        $this->url = $url;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): InterfaceSymfony
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return NamespaceSymfony|null
     */
    public function getNamespaceSymfony(): NamespaceSymfony
    {
        return $this->namespaceSymfony;
    }

    public function setNamespaceSymfony(NamespaceSymfony $namespaceSymfony): InterfaceSymfony
    {
        $this->namespaceSymfony = $namespaceSymfony;

        return $this;
    }
}
