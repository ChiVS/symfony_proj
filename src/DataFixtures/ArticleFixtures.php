<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $article = new Article();
        $article->setName('Test One');
        $article->setDescription('Fixture');
        $article->setCreatedAt(new \DateTime('now'));
        $manager->persist($article);

        $article = new Article();
        $article->setName('Test Two');
        $article->setDescription('Fixture');
        $article->setCreatedAt(new \DateTime('now'));
        $manager->persist($article);

        $manager->flush();
    }
}
