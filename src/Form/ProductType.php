<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
        'label' => 'label.product.name',
        'attr' => [
            'placeholder' => 'Enter the name of the product',
            ],
        ])
            ->add('price', MoneyType::class, [
                'scale' => 2,
                'label' => 'label.product.price',
                'attr' => [
                    'min' => '0.00',
                    'max' => '1000.00',
                    'step' => '0.01',
                    'placeholder' => 'Enter the cost of the product',
                ],
            ])
            ->add('category', TextType::class, [
                'label' => 'label.product.category',
                'attr' => [
                    'placeholder' => 'Enter product category',
                ],
            ])
            ->add('height', IntegerType::class, [
                'label' => 'label.product.height',
                'attr' => [
                    'placeholder' => 'Overall dimensions: height',
                ],
            ])
            ->add('width', IntegerType::class, [
                'label' => 'label.product.width',
                'attr' => [
                    'placeholder' => 'Overall dimensions: width',
                ],
            ])
            ->add('depth', IntegerType::class, [
                'label' => 'label.product.depth',
                'attr' => [
                    'placeholder' => 'Overall dimensions: depth',
                ],
            ])
            ->add('weight', NumberType::class, [
                'label' => 'label.product.weight',
                'attr' => [
                    'placeholder' => 'Product weight',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'label.product.save',
            ])
            ->add('reset', ResetType::class, [
                'label' => 'label.product.reset',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
