<?php

namespace App\Command;

use App\Entity\ClassSymfony;
use App\Entity\InterfaceSymfony;
use App\Entity\NamespaceSymfony;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Class ParseNamespacesCommand.
 */
class ParseSymfonyCommandNSRec extends Command
{
    /** @var string */
    private const API_URL = 'http://api.andreybolonin.com/';

    /** @var string */
    protected static $defaultName = 'app:parsesymfonynsrec';

    /** @var EntityManagerInterface */
    private $em;

    /**
     * ParseNamespacesCommand constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setName(self::$defaultName)
            ->setDescription('Parsing site api.andreybolonin.com')
            ->setHelp('This command parses the site ...')
            ->setDefinition(new InputDefinition(
                [
                    new InputOption('test', 't', InputOption::VALUE_OPTIONAL, 'none', false),
                ]
            ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $test = $input->getParameterOption('--test');
//        if (null == $test) {
//            $test = true;
//        } else {
//            $test = false;
//        }

        if ($test) {
            $output->writeln('command executed');

            return 1;
        }

        $namespace = new NamespaceSymfony();
        $namespace->setUrl(self::API_URL.'Symfony.html');
        $namespace->setName('Symfony');
        $this->em->persist($namespace);
        $this->getContent(self::API_URL.'Symfony.html', $namespace, $test);

        $output->writeln('command executed');

        return 1;
    }

    private function getContent($url, NamespaceSymfony $parent, $test)
    {
        $httpClient = HttpClient::create();
        $html = $httpClient->request('GET', $url);
        $crawler = new Crawler($html->getContent());

        $interfaces = $crawler->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > em > a > abbr');
        foreach ($interfaces as $interface) {
            $urlInterface = self::API_URL.str_replace('\\', '/', $interface->getAttribute('title').'.html');
            $newInterface = new InterfaceSymfony();
            $newInterface->setName($interface->textContent);
            $newInterface->setUrl($urlInterface);
            $newInterface->setNamespaceSymfony($parent);
            $this->em->persist($newInterface);
            $this->em->flush();
        }

        $classes = $crawler->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > a');
        foreach ($classes as $class) {
            $urlClasses = self::API_URL.str_replace('../', '', $class->getAttribute('href'));
            $newClass = new ClassSymfony();
            $newClass->setName($class->textContent);
            $newClass->setUrl($urlClasses);
            $newClass->setNamespaceSymfony($parent);
            $this->em->persist($newClass);
            $this->em->flush();
        }

        $namespaces = $crawler->filter('div#page-content > div.namespace-list > a');
        foreach ($namespaces as $namespace) {
            $urlNamespace = self::API_URL.str_replace('../', '', $namespace->getAttribute('href'));
            $newNamespace = new NamespaceSymfony();
            $newNamespace->setName($namespace->textContent);
            $newNamespace->setUrl($urlNamespace);
            $newNamespace->setParent($parent);
            $this->em->persist($newNamespace);
            $this->em->flush();

            $this->getContent($urlNamespace, $newNamespace, $test);
        }
    }
}
