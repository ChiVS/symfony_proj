<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClrDBCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:clrdb')
            ->setDescription('Clear Data Base')
            ->setHelp('This command Clear Data Base on my Project');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        echo shell_exec('bin/console d:s:d --force');
        echo shell_exec('bin/console d:s:u --force');
        echo shell_exec('bin/console d:s:v');
        echo shell_exec('bin/console d:f:l --no-interaction');

        return 1;
    }
}
