<?php

namespace App\Command;

use App\Entity\NamespaceSymfony;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ParseSymfonyCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /** @var string */
    private const API_URL = 'http://api.andreybolonin.com/';

    /** @var string */
    protected static $defaultName = 'app:parsesymfony';

    /**
     * ParseSymfonyCommand constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Parsing site api.andreybolonin.com')
            ->setHelp('This command parses the site ...')
            ->addOption('test', 't', InputOption::VALUE_OPTIONAL, 'Test', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $test = $input->getParameterOption('--test');

        $html = file_get_contents(self::API_URL);

        $crawler = new Crawler($html);
        $filtered = $crawler->filter('div.namespace-container > ul > li > a');

        foreach ($filtered as $value) {
            $url = self::API_URL.str_replace('../', '', $value->getAttribute('href'));
            $set = new NamespaceSymfony();
            $set->setName($value->textContent);
            $set->setUrl($url);
            $this->em->persist($set);

            if ($test) {
                $output->writeln('command executed');

                return 1;
            }
        }

        $this->em->flush();
        $output->writeln('command executed');

        return 1;
    }
}
