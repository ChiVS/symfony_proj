<?php

namespace App\Command;

use App\Entity\ClassSymfony;
use App\Entity\InterfaceSymfony;
use App\Entity\NamespaceSymfony;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Class ParseNamespacesCommand.
 */
class ParseSymfonyCommandNS extends Command
{
    /** @var string */
    private const API_URL = 'http://api.andreybolonin.com/';

    /** @var string */
    protected static $defaultName = 'app:parsesymfonyns';

    /** @var EntityManagerInterface */
    private $em;

    /**
     * ParseNamespacesCommand constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setName(self::$defaultName)
            ->setDescription('Parsing site api.andreybolonin.com')
            ->setHelp('This command parses the site ...')
            ->addOption('test', 't', InputOption::VALUE_OPTIONAL, 'Test', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $test = $input->getParameterOption('--test');

//        $namespace = new NamespaceSymfony();
//        $namespace->setUrl(self::API_URL);
//        $namespace->setName('Symfony');
//        $this->em->persist($namespace);
//        $this->em->flush();

        if ($test) {
            $output->writeln('command executed');

            return 1;
        }

        $httpClient = HttpClient::create();
        $html = $httpClient->request('GET', self::API_URL);
        $crawler = new Crawler($html->getContent());

        $namespaces = $crawler->filter('div.namespace-container > ul > li > a');

        foreach ($namespaces as $namespace) {
            $urlNamespace = self::API_URL.str_replace('../', '', $namespace->getAttribute('href'));
            $newNamespace = new NamespaceSymfony();
            $newNamespace->setName($namespace->textContent);
            $newNamespace->setUrl($urlNamespace);
//            $parent = NamespaceSymfony::getId($newNamespace);
//            $newNamespace->setParent($parent);
            $this->em->persist($newNamespace);
            $this->em->flush();

            $httpClient = HttpClient::create();
            $html = $httpClient->request('GET', $urlNamespace);
            $crawler = new Crawler($html->getContent());

            $interfaces = $crawler->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > em > a');
            foreach ($interfaces as $interface) {
                $urlInterface = self::API_URL.str_replace('../', '', $interface->getAttribute('href'));
                $newInterface = new InterfaceSymfony();
                $newInterface->setName($interface->textContent);
                $newInterface->setUrl($urlInterface);
                $newInterface->setNamespaceSymfony($newNamespace);
                $this->em->persist($newInterface);
                $this->em->flush();
            }

            $classes = $crawler->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > a');
            foreach ($classes as $class) {
                $urlClass = self::API_URL.str_replace('../', '', $class->getAttribute('href'));
                $newClass = new ClassSymfony();
                $newClass->setName($class->textContent);
                $newClass->setUrl($urlClass);
                $newClass->setNamespaceSymfony($newNamespace);
                $this->em->persist($newClass);
                $this->em->flush();
            }
        }

//        $this->em->flush();

        $output->writeln('command executed');

        return 1;
    }
}
