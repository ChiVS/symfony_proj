<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class SecurityControllerTest.
 */
class SecurityControllerTest extends WebTestCase
{
    public function testRegisterAction()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $crawler = $client->request('GET', '/register');
        $button = $crawler->selectButton('user_create[submit]');
        $form = $button->form([
            'user_create[username]' => 'usertest',
            'user_create[password][first]' => 'test123',
            'user_create[password][second]' => 'test123',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testLoginAction()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/en/login/');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'usertest',
            'password' => 'test123',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testIndexAction()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/en/login/');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'usertest',
            'password' => 'test123',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/en');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateAction()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/en/login/');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'usertest',
            'password' => 'test123',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/en');
        $crawler = $client->request('GET', '/article/en/create');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'test create user',
            'article[description]' => 'create fake article description',
            'article[created_at][date][month]' => '3',
            'article[created_at][date][day]' => '23',
            'article[created_at][date][year]' => '2021',
            'article[created_at][time][hour]' => '22',
            'article[created_at][time][minute]' => '47',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testEditActionAdmin()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/en/login/');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);

        $client->submit($form);

        $client->request('GET', '/article/en');

        $crawler = $client->request('GET', '/article/en/edit/2');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'test edited admin',
            'article[description]' => 'edit fake article description',
            'article[created_at][date][month]' => '5',
            'article[created_at][date][day]' => '5',
            'article[created_at][date][year]' => '2021',
            'article[created_at][time][hour]' => '9',
            'article[created_at][time][minute]' => '40',
        ]);

        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testEditActionUser()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/en/login/');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'usertest',
            'password' => 'test123',
        ]);

        $client->submit($form);

        $client->request('GET', '/article/en');

        $crawler = $client->request('GET', '/article/en/edit/2');

        $this->assertEquals(403, $client->getResponse()->getStatusCode());

    }

    public function testDeleteArticleAdmin()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/en/login/');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/en');

        $client->request('GET', '/article/en/delete/1');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteArticleUser()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/en/login/');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'usertest',
            'password' => 'test123',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/en');

        $client->request('GET', '/article/en/delete/2');
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testSendEmail()
    {
//        $this->markTestSkipped();
        $client = static::createClient();
        $client->request('GET', '/email');
        $this->assertEmailCount(1);
    }

}
