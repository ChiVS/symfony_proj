<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest.
 */
class XlsHtmlPdfTest extends WebTestCase
{
    public function testPdf()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $client->request('GET', '/pdf');

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'application/pdf');
    }

    public function testXls()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $client->request('GET', '/xls');

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }

    public function testQr()
    {
//        $this->markTestSkipped();
        $client = static ::createClient();
        $client->request('GET', '/qr');
        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'image/png');
    }
}
