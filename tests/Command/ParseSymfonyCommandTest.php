<?php

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ParseSymfonyCommandTest extends KernelTestCase
{
    public function testExecuteOne()
    {
//        $this->markTestSkipped();
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:parsesymfony');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--test' => '1']);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('command executed', $output);
    }

    public function testExecuteTwo()
    {
//        $this->markTestSkipped();
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:parsesymfonyns');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--test' => '1']);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('command executed', $output);
    }

    public function testExecuteThree()
    {
//        $this->markTestSkipped();
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:parsesymfonynsrec');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--test' => '1']);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('command executed', $output);
    }
}
